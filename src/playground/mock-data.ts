import { Product } from './view-model';

export const mockProducts: Product[] = [
  {
    id: 1,
    name: `Chai`,
    price: 18,
    stock: 39,
    categoryName: 'Beverages',
    categoryDescription: 'Soft drinks, coffees, teas, beers, and ales',
  },
  {
    id: 2,
    name: `Chang`,
    price: 19,
    stock: 17,
    categoryName: 'Beverages',
    categoryDescription: 'Soft drinks, coffees, teas, beers, and ales',
  },
  {
    id: 3,
    name: `Aniseed Syrup`,
    price: 10,
    stock: 13,
    categoryName: 'Condiments',
    categoryDescription:
      'Sweet and savory sauces, relishes, spreads, and seasonings',
  },
  {
    id: 4,
    name: `Chef Anton's Cajun Seasoning`,
    price: 22,
    stock: 53,
    categoryName: 'Condiments',
    categoryDescription:
      'Sweet and savory sauces, relishes, spreads, and seasonings',
  },
  {
    id: 5,
    name: `Chef Anton's Gumbo Mix`,
    price: 21.35,
    stock: 0,
    categoryName: 'Condiments',
    categoryDescription:
      'Sweet and savory sauces, relishes, spreads, and seasonings',
  },
  {
    id: 6,
    name: `Grandma's Boysenberry Spread`,
    price: 25,
    stock: 120,
    categoryName: 'Condiments',
    categoryDescription:
      'Sweet and savory sauces, relishes, spreads, and seasonings',
  },
  {
    id: 7,
    name: `Uncle Bob's Organic Dried Pears`,
    price: 30,
    stock: 15,
    categoryName: 'Produce',
    categoryDescription: 'Dried fruit and bean curd',
  },
  {
    id: 8,
    name: `Northwoods Cranberry Sauce`,
    price: 40,
    stock: 6,
    categoryName: 'Condiments',
    categoryDescription:
      'Sweet and savory sauces, relishes, spreads, and seasonings',
  },
  {
    id: 9,
    name: `Mishi Kobe Niku`,
    price: 97,
    stock: 29,
    categoryName: 'Meat/Poultry',
    categoryDescription: 'Prepared meats',
  },
  {
    id: 10,
    name: `Ikura`,
    price: 31,
    stock: 31,
    categoryName: 'Seafood',
    categoryDescription: 'Seaweed and fish',
  },
];
