import * as React from 'react';
import {
  MultiGrid as VirtualizedMultiGrid,
  GridCellProps,
} from 'react-virtualized';
import { cellStyles, headerStyles } from './multi-grid.styles';
import { Column } from './multi-grid.vm';
import {
  getRowIndex,
  getColumnKey,
  getHeaderText,
} from './multi-grid.business';

interface GridHeaderProps {
  columnIndex: number;
  rowIndex: number;
}

interface Props {
  rows: any[];
  columns: Column[];
  width: number;
  fixedRowCount: number;
  columnCount: number;
}

export const MultiGrid: React.FunctionComponent<Props> = props => {
  const { rows, columns, width, fixedRowCount, columnCount } = props;

  const rowCount = columns ? rows.length + fixedRowCount : rows.length;

  return (
    <VirtualizedMultiGrid
      cellRenderer={cellProps => (
        <Cell
          {...cellProps}
          rows={rows}
          columns={columns}
          fixedRowCount={fixedRowCount}
        />
      )}
      columnWidth={width / columnCount}
      columnCount={columnCount}
      height={300}
      rowHeight={40}
      rowCount={rowCount}
      width={width}
      fixedRowCount={fixedRowCount}
      style={{
        border: '1px solid #eee',
      }}
      styleTopRightGrid={{
        borderBottom: '2px solid #eee',
      }}
    />
  );
};

MultiGrid.defaultProps = {
  width: 0,
  fixedRowCount: 0,
};

interface CellProps extends GridCellProps {
  rows: any[];
  columns?: Column[];
  fixedRowCount: number;
}

const Cell: React.FunctionComponent<CellProps> = props => {
  const { rows, columns, fixedRowCount, columnIndex, style } = props;

  const hasColumns = Boolean(columns);
  const isColumn = hasColumns && props.rowIndex <= fixedRowCount - 1;
  const rowIndex = getRowIndex(
    props.rowIndex,
    fixedRowCount,
    hasColumns,
    isColumn
  );

  const columnKey = getColumnKey(rows[rowIndex], columnIndex, isColumn);

  return isColumn ? (
    <Header
      style={style}
      columns={columns}
      columnIndex={columnIndex}
      rowIndex={rowIndex}
    />
  ) : (
    <div className={cellStyles} style={style}>
      {rows[rowIndex][columnKey]}
    </div>
  );
};

interface HeaderProps {
  columns: Column[];
  columnIndex: number;
  rowIndex: number;
  style: React.CSSProperties;
}

const Header: React.FunctionComponent<HeaderProps> = props => {
  const { columns, columnIndex, rowIndex, style } = props;
  const columnText = getHeaderText(columns, columnIndex, rowIndex);
  return (
    <div className={headerStyles(columnText)} style={style}>
      {getHeaderText(columns, columnIndex, rowIndex)}
    </div>
  );
};
