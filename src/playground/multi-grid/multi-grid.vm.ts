export interface Column {
  name: string;
  columnIndex: number;
  rowIndex: number;
}
