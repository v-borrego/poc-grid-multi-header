import { css } from 'emotion';

export const cellStyles = css`
  display: flex;
  align-items: center;
  justify-content: center;
  border-bottom: 1px solid #eee;
  border-right: 1px solid #eee;
`;

export const headerStyles = (columnText: string) => css`
  ${cellStyles}
  border-bottom: ${columnText ? '1px solid #eee' : 'none'};
`;
