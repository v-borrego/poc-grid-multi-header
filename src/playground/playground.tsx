import * as React from 'react';
import { AutoSizer, GridCellProps } from 'react-virtualized';
import { MultiGrid } from './multi-grid/multi-grid';
import { Product } from './view-model';
import { mockProducts } from './mock-data';

export const Playground: React.FunctionComponent = props => {
  const [products, setProducts] = React.useState<Product[]>([]);

  React.useEffect(() => {
    setProducts(mockProducts);
  }, []);

  return (
    <AutoSizer disableHeight>
      {({ width }) => (
        <MultiGrid
          rows={products}
          width={width}
          columns={[
            { name: 'ID', columnIndex: 0, rowIndex: 2 },
            { name: 'Product Information', columnIndex: 1, rowIndex: 0 },
            { name: 'Name', columnIndex: 1, rowIndex: 2 },
            { name: 'Unit', columnIndex: 2, rowIndex: 1 },
            { name: 'Price', columnIndex: 2, rowIndex: 2 },
            { name: 'In Stock', columnIndex: 3, rowIndex: 2 },
            { name: 'Category', columnIndex: 4, rowIndex: 0 },
            { name: 'Name', columnIndex: 4, rowIndex: 2 },
            { name: 'Description', columnIndex: 5, rowIndex: 2 },
          ]}
          fixedRowCount={3}
          columnCount={6}
        />
      )}
    </AutoSizer>
  );
};
